import sys
import math
import random
import time
import itertools as it
import functools as func

import tensorflow.keras as keras
import numpy as np

import nfl_strategy as nfl

import test_qfl

from bandit import Bandit


def eps_greedy(eps, n):
    ''' Returns a function that takes a Q-function and returns a function that, given
        a position, returns a randomly chosen action numbered 0 to n-1
        with probability eps and returns the
        choice with the maximum Q-value at that position otherwise.

        eps -- a real number between 0.0 and 1.0 inclusive
        n -- a positive integer
    '''
    def fxn(q):
        def choose(pos):
            if random.random() < eps:
                return random.randrange(0, n)
            else:
                return max(enumerate([q(pos, a) for a in range(n)]), key=lambda p: p[1])[0]
        return choose
    return fxn


def q_learn(model, time_limit):
    ''' Returns a policy for QFL.  A policy is a function that takes a
        position and returns the index of the selected offensive play.

        model -- the model of a game
        time_limit -- a positive integer
    '''
    # we ignore the time limit here
    return deep_q(model, 10000, eps_greedy(0.225, model.offensive_playbook_size()), 0.1, 0.99, 2, 20, 20)


def set_optimizer(model, alpha):
    ''' Sets the optimizer for the given model to a SGD optimizer with
        the given learning rate.

        model -- a Keras model
        alpha -- a number between 0.0 and 1.0
    '''
    sgd = keras.optimizers.SGD(lr=alpha, decay=1e-6, momentum=0.8, nesterov=True)
    
    model.compile(loss="mse", optimizer=sgd)


def make_model(game, alpha):
    ''' Returns a Keras model for approximating Q(s, a) values for
        NFL strategy.  The model will have 7 inputs: 4 for the components
        of the position (yard line, downs left, yards to first down, time)
        and 3 inputs for the one-hot representation of the action.  The
        model will have a single output giving the estimate of the Q
        value.

        game -- the model of the game
        alpha -- the initial learning rate of the model
    '''
    model = keras.models.Sequential()
    model.add(keras.layers.Dense(25, activation='relu', input_dim = 4 + game.offensive_playbook_size()));
    model.add(keras.layers.Dropout(0.1))
    model.add(keras.layers.Dense(1, activation="linear"))
    set_optimizer(model, alpha)
    return model


def deep_q(game, episodes, select, alpha, gamma, train_interval, transfer_interval, batch_size):
    ''' Returns a policy for the given game.  The policy will be trained using
        Q-learning with an artificial neural network as the function
        approximator.

        game -- the model of the game
        episodes -- a positive integer giving the number of episodes to train
        select -- the function that, given Q function, returns the function
        to use to select actions during training (for example, epsilon-greedy)
        alpha -- the initial learning rate (decay is currently hard-coded)
        gamma -- the discount on future rewards
        train_interval -- the number of episodes between training the ANN
        transfer_interval -- the number of episodes between copying the
        learning network to the target network
        batch_size -- the number of training examples to train on during
        each training episode
    '''
    playbook_size = game.offensive_playbook_size()

    # chosen so that the final learning rate after 5000 training episodes
    # is on the order of 1/100 of the original (hmmm, if we know what
    # we want the final learning rate to be, we could probably figure
    # out the exact decay to use here)
    decay = 0.999

    # make the initial models
    learning = make_model(game, alpha)
    target = make_model(game, alpha)

    # initialize a multi-armed bandit for tracking best candidates
    plays_per_copy = 400
    bandit = Bandit(plays_per_copy, 2)

    # our function to translate between game positions and ANN inputs
    def encode_state(s):
        # normalize each component to the range [0.0, 1.0]
        return [s[0] / 100.0, s[1] / 4.0, s[2] / 20.0, s[3] / 24.0]

    # our function to translate actions to ANN inputs
    def encode_action(a):
        # one-hot encoding of the action
        action_vec = [0] * playbook_size
        action_vec[a] = 1
        return action_vec

    # the Q function
    def q(network, s, a):
        if game.game_over(s):
            # don't need to approximate for terminal positions
            return 0
        else:
            input_vec = np.matrix(encode_state(s) + encode_action(a))
            return network(input_vec)[0][0].numpy()

    def make_policy(model):
        ''' Returns a policy function that uses the given ANN to choose
            the best action.

            model -- a Keras model
        '''
        def policy(pos):
            # find a that maximizes Q(s, a)
            return max(enumerate([q(model, pos, a) for a in range(playbook_size)]), key=lambda p: p[1])[0]
        return policy

    # apply the given selection function to our Q function
    select = select(lambda s, a: q(learning, s, a))

    # initialize the partitioned replay database -- the database is partitioned
    # using the given number of bins for each component; we do this to
    # avoid overrepresenting positions near the initial posiion in the
    # positions sampled for training
    part_size = (5, 1, 1, 2)
    part_count = func.reduce(lambda x, y: x * y, part_size)
    replay = {part: [] for part in it.product(*[range(n) for n in part_size])}

    for e in range(episodes):
        print("episode ", e)

        # one episode = one game simulated from start to finish
        curr = game.initial_position()
        while not game.game_over(curr):
            # select an action
            a = select(curr)

            # observe the result
            succ, _ = game.result(curr, a)

            # compute the reward
            r = (1.0 if game.win(succ) else -1.0) if game.game_over(succ) else 0.0

            # determine appropeiate part of the replay database partition
            state = encode_state(curr)
            curr_part = tuple([min(part_size[i] - 1, int(state[i] * part_size[i])) for i in range(len(part_size))])

            # add this play to the database
            replay[curr_part].append((curr, a, succ, r))
            
            curr = succ

        # train network
        if (e + 1) % train_interval == 0:
            # sample evenly from each part of the partiioned replay database
            samples = []
            for part in replay:
                samples.extend(random.sample(replay[part], min(len(replay[part]), batch_size // part_count)))

            # encode sampled replays as ANN inputs
            x_train = [encode_state(curr) + encode_action(a) for curr, a, _, _ in samples]

            # compute expected outputs
            y_train = [[max(-1, min(1, r + gamma * max(q(target, succ, a) for a in range(playbook_size))))] for _, _, succ, r in samples]
            
            for example in zip(x_train, y_train):
                print(example)

            # train the ANN
            learning.fit(np.matrix(x_train), np.matrix(y_train), epochs=1, batch_size=10)

            # manually decay the learning rate
            lr = alpha * math.pow(decay, e / train_interval)
            print("learning rate =", lr)
            set_optimizer(learning, lr)

        # copy trained to target
        if (e + 1) % transfer_interval == 0:
            value = game.simulate(make_policy(learning), plays_per_copy)
            print("value =", value)

            bandit.play(lambda model: game.simulate(make_policy(model),
                                                    plays_per_copy))
            print("best value =", bandit.best_value())

            if value > game.simulate(make_policy(target), plays_per_copy):
                print("copying")
                target.set_weights(learning.get_weights())
                if (value > bandit.best_value()):
                    # add a new arm to the bandit
                    arm = make_model(game, alpha)
                    arm.set_weights(learning.get_weights())
                    bandit.add(arm, value, 1)

    print(bandit.best_lower()[1:])
    return make_policy(bandit.best_lower()[0])

import sys
import csv

import numpy as np

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.optimizers.schedules import ExponentialDecay

def main():
    x_all = [] #inputs
    y_all = [] #outputs

    # read the data
    reader = csv.reader(sys.stdin)
    for row in reader:
        # split into inputs...
        x_all.append([float(x) for x in row[:4]])

        # ...and output, which is converted to three booleans
        if row[-1] == "Iris-setosa":
            y_all.append([1, 0, 0])
        elif row[-1] == "Iris-versicolor":
            y_all.append([0, 1, 0])
        else:
            y_all.append([0, 0, 1])

    # normalize the data
    mins = [0.0] * 4
    maxes = [0.0] * 4
    for i in range(0, 4):
        mins[i] = min([x[i] for x in x_all])
        maxes[i] = max([x[i] for x in x_all])

    x_norm = [[(x[i] - mins[i]) / (maxes[i] - mins[i]) * 1.0 + 0.0 for i in range(0, 4)] for x in x_all]

    # split into training data and test data
    test_size = int(len(x_norm) / 5)
    train_size = len(x_norm) - test_size

    x_train = np.matrix(x_norm[:train_size])
    y_train = np.matrix(y_all[:train_size])

    x_test = x_norm[train_size:]
    y_test = y_all[train_size:]

    # set the topology of the neural network
    model = Sequential()
    model.add(Dense(10, activation="relu", input_dim = x_train.shape[1]))
    model.add(Dropout(0.1))
    model.add(Dense(y_train.shape[1], activation = "softmax"))

    # set up optimizer
    lr_schedule = ExponentialDecay(initial_learning_rate=0.1, decay_rate=0.9, decay_steps=100)
    sgd = SGD(learning_rate=lr_schedule, momentum=0.8, nesterov=True)
    model.compile(loss="categorical_crossentropy", optimizer=sgd)

    # train!
    model.fit(x_train, y_train, epochs=200, batch_size=5)

    # get predictions, convert to class 0, 1, 2, and compare to test data
    y_predict = [max(list(zip(range(3), y)), key=lambda x:x[1])[0]\
                 for y in model.predict(np.matrix(x_test))]
    y_correct = [max(list(zip(range(3), y)), key=lambda x:x[1])[0]\
                 for y in y_test]
    print(sum([(1 if y[0] == y[1] else 0) for y in zip(y_predict, y_correct)]) / len(y_predict))

if __name__ == "__main__":
    main()
    

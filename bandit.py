import math

class Bandit:
    def __init__(self, n, c):
        ''' Initializes an empty multi-armed bandit where each arm
            is a binomial distribution.  The bandit thus
            uses an upper confidence bound based on the observed mean
            and observed standard deviation.
        
            n -- the number of samples per "play" of each arm, so that
                 the estimate of the standard deviation of the observed
                 mean over all samples for one arm is the standard
                 deviation of a binomial distibution with the observed
                 mean divided by sqrt(n * count)
            c -- the number of standard deviations above the mean to
                 use as the UCB
        '''
        self._arms = []
        self._n = n
        self._c = c


    def best(self):
        ''' Returns a triple (object, mean, count) for the arm with
            the highest observed mean in this machine.
        '''
        if len(self._arms) == 0:
            return None
        else:
            return max(self._arms, key=lambda t:t[1])


    def best_lower(self):
        ''' Returns a triple (object, mean, count) for the arm with
            the highest lower confidence bound
        '''
        if len(self._arms) == 0:
            return None
        else:
            return max(self._arms, key=lambda p: self._ucb(p, -1))


    def best_value(self):
        ''' Returns the highest observed mean among all arms. '''
        if len(self._arms) == 0:
            return -float("inf")
        else:
            return max(mean for _, mean, _ in self._arms)


    def add(self, obj, mean, count):
        ''' Adds the given arm with given initial mean and play count
            to this bandit.

            obj -- the object corresponding to the new arm
            mean -- a number between 0.0 and 1.0
            count -- a positive integer
        '''
        self._arms.append((obj, mean, count))


    def play(self, f):
        ''' Plays an arm according to the current upper confidence bounds.
            The arm is evaluated by passing the corresponding object to the
            given function.
 
            f -- a function that takes the object on the arms in this
                 machine and returns a float for its score
        '''
        if len(self._arms) > 0:
            i = max(enumerate(self._arms), key=lambda p: self._ucb(p[1]))[0]
            obj, mean, count = self._arms[i]
            score = f(obj)
            self._arms[i] = (obj, (mean * count + score) / (count + 1), count + 1)


    def _ucb(self, arm, sign=1):
        ''' Computes the upper confidence bound for the given arm.

            arm -- a triple (object, mean, count)
            sign -- +1 or -1 to indicate upper or lower confidence bound
                    respectively
        '''
        obj, mean, count = arm
        print("UCB selecting", mean, count)
        return mean + sign * self._c * math.sqrt(mean * (1 - mean)) / math.sqrt(self._n * count)

    
    def count(self):
        ''' Returns the number of arms in this bandit. '''
        return len(self._arms)
